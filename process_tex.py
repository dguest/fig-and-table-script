#!/usr/bin/env python3

import getopt,os,sys,string,re,shutil,subprocess
from shutil import copy,copyfile,copytree
from glob import glob

import sre_compile

print('Running digest script for tex files')
print('Just as a reminder, I am not going to handle brackets {}, even escaped brackets, in figure names.')
print('Credits: script written my Zach Marshall (LBNL), for the benefit of the ATLAS Collaboration')
print('Copied for DUNE, with permission, and migrated to Python 3 by Frank Filthaut')
# print('Thanks to Marcello Barisonzi for implementing macro replacement.')
# print('Thanks to Rainer Bartoldus for a couple of nice additions')
# print('Thanks to Steven Schramm for some very nice macro (with argument) processing code!!')
# print('Thanks to Davide Gerbaudo for implementing reference counting!')
# print('Thanks to TJ Khoo for several fixes and getting table inputting (at least partly) working again')
print('')
print('Please remember that for CONF notes, you should not have "Auxiliary material" - so you want the option -x')
print('  ... I will try to handle that automatically.')
print('')
# print('Table extraction is now on by default.')
# print('')


if sys.version_info.major<3:
    print('Please run in python3!')
    sys.exit(1)
if sys.version_info.minor<7:
    print('Requires Python 3.7 or newer. In ATLAS, try:')
    print('lsetup "python 3.7.4-x86_64-centos7"')
    sys.exit(1)

def usage():
    print(' ')
    print('Usage: top_script.py inputFile [options]')
    print('  -i | --input   : input file')
    print('  -o | --output  : output directory')
    print('  -n | --number  : starting number (default 1)')
    print('  -a | --aux     : auxiliary material')
    print('  -x | --noAux   : do not change numbering to aux on finding appendices')
    print('  -c | --hundred : Use this option if you expect to have more than 99 figures')
    print('  -t | --notables: DONT process tables (default is to include tables)')
    print('  -T | --tabletex: Save table tex for debugging')
    print('  -u | --captions: include captions in output tables (experimental)')
    print('  -m | --multifilecopy: also copy files with the same base name but different extensions (default is only the specifically used figure)')
    print('  -d | --double  : Double expand macros; needed for some more complicated files')
    print('  -s | --chapters: Split figures and tables by chapter (for TDRs). Also disables png and jpeg warnings')
    print('  -f | --find    : Super fall-back option to use find command when figures cannot be located')
    print('  -v | --verbose : print more information')
    print('  -? | --usage   : print this help message')
    print('  -h | --help    : print this help message')
    print(' ')

verbose=False
inputFile = '/tmp/zmarshal/Razor/razor-epjc.tex'
outputFile = '/tmp/zmarshal/script_results/'
number = 1
aux = False
noaux = False
hundred = 2
tables = True
inc_cap = False
multifilecopy = False
double = False
tabletex = False
chapters = False
findall = False
failed_keys = []

try:
    # retrieve command line options
    shortopts  = "n:i:o:axtTudsvcmfh?"
    longopts   = ["number=","verbose","help", "usage","input=","output=",'aux','noAux','hundred','notables','tabletex','captions','double','multifilecopy','chapters','find']
    opts, args = getopt.getopt( sys.argv[1:], shortopts, longopts )

except getopt.GetoptError:
    # print help information and exit:
    print('ERROR: unknown options in argument {}'.format(sys.argv[1:]), file = sys.stderr)
    usage()
    sys.exit(1)

for o, a in opts:
    if o in ("-?", "-h", "--help", "--usage"):
        usage()
        sys.exit(0)
    if o in ("-v","--verbose"):
        verbose = True
    if o in ('-n','--number'):
        number=int(a)
    if o in ('-i','--input'):
        inputFile=a
    if o in ('-o','--output'):
        outputFile=a
    if o in ('-a','--aux'):
        aux=True
    if o in ('-x','--noaux'):
        noaux=True
    if o in ('-c','--hundred'):
        hundred=3
    if o in ('-t','--notables'):
        tables=False
    if o in ('-T','--tabletex'):
        tabletex=True
    if o in ('-u','--captions'):
        inc_cap=True
    if o in ('-m','--multifilecopy'):
        multifilecopy=True
    if o in ('-d','--double'):
        double = True
    if o in ('-s','--chapters'):
        chapters = True
    if o in ('-f','--find'):
        findall = True

print('Configured to run on input file {}'.format(inputFile))
print('Writing output to directory {}'.format(outputFile))
print('Will begin numbering at {:1d}'.format(number))
print('Will use {:1d} digits for figure numbers'.format(hundred))
if multifilecopy: print('Will copy all files with the same name but different extensions')
if chapters: print('Will add chapter prefix')
if verbose: print('And running with verbosity switched on')
if findall: print('Will use system find command to locate figures')
print('\n\n')


def guess_aux_filename(inputFile=''):
    return inputFile.replace('.tex', '.aux')

def parse_labels(auxFilename='', verbose=False):
    """get a dict of {'label':'string'} values where 'string' is the
    string that appears in the output pdf for a given label.
    Labels are expected to be a line
    \newlabel{<labelname>}{{<labelvalue>}<...other stuff...>}
    in the aux file."""
    if verbose:
        print('trying to parse labels from {}'.format(auxFilename))
    if not os.path.exists(auxFilename):
        print('invalid file {}'.format(auxFilename))
        return {}
    else:
        lrex = re.compile(r'\\newlabel\{(?P<labelname>.*?)\}\{\{(?P<labelvalue>.*?)\}') # 2 non-greedy named patterns (literal)
        aux_text = [l for l in open(auxFilename).readlines() if 'newlabel' in l]
        matches = [lrex.match(l) for l in aux_text]
        matches = [m for m in matches if m]
        return dict((m.group('labelname'), m.group('labelvalue')) for m in matches)


class macro_with_args:
    
    def __init__(self,key_string,argument_string,value_string):
        # Store input values
        self.keyString = key_string.strip()
        self.argString = argument_string
        self.valString = value_string

        # Ensure the argument string makes sense
        if self.argString.count('[') != self.argString.count(']') or self.argString.count('{') != self.argString.count('"'):
            print("ERROR: unexpected argument format of {}".format(self.argString))
            return None

        # Determine the number of arguments, store default arguments if applicable
        argTokens = re.sub('\[','',self.argString).split(']')
        if len(argTokens) > 1:
            self.hasDefaultArgs = True
        else:
            self.hasDefaultArgs = False

        self.numArgs = int(argTokens[0])
        self.defaultArgs = None if not self.hasDefaultArgs else argTokens[1:-1] # -1 as split() appends the empty string to the end if the string ends with the delimiter
        if self.hasDefaultArgs and self.numArgs < len(self.defaultArgs):
            print("ERROR: Provided more default argument values than arguments, {:d} vs {:d}, for {}".format(len(self.defaultArgs),self.numArgs,self.keyString))
            return None

        # Build some search expressions for later
        self.macroDefNew = re.compile("newcommand\*?{\s*%s\s*}"%(self.keyString))
        self.macroDefDRC = re.compile("DeclareRobustCommand\*?{\s*%s\s*}"%(self.keyString))

        self.macroSubDefNew = re.compile("newcommand\*?{.*}\s*(\[[0-9]*\])*\s*{.*%s.*}"%(self.keyString))
        self.macroSubDefDRC = re.compile("DeclareRobustCommand\*?{.*}\s*(\[[0-9]*\])*\s*{.*%s.*}"%(self.keyString))

        # We now have the number of arguments and their default values
        # This is all we need to figure out for the time being


    def __str__(self):
        return self.valString

    def replace_sub_macros(self,macro_dict,verbose):
        # Check if the macro contains sub-macros
        # If so, replace them directly here
        # Do it once now (per macro) to avoid repetitions later
        # Note that this might have to be done multiple times for very nested macros
        # If it was updated, try to update it again
        
        temp = replaceMacros(macro_dict,self.valString,verbose)
        updated = temp != self.valString
        while updated:
            temp2 = replaceMacros(macro_dict,temp,verbose)
            updated = temp2 != temp
            temp = temp2
        
        if self.valString != temp:
            if verbose:
                print("Updated macro {}: {} --> {}".format(self.keyString,self.valString,temp))
            self.valString = temp
                

    def process_line_helper(self,aline,keyResult):
        
        lineFromKey = aline[keyResult.start():]
        
        # Parse the arguments
        delimiterStack = []
        arguments      = []
        numDefaultArgs = 0
        numArgChars    = 0
        for iChar in range(len(self.keyString)-1,len(lineFromKey)):
            nextChar = lineFromKey[iChar]
            if len(delimiterStack) == 0:
                # Check if the next character starts a new argument
                if nextChar == '[' or nextChar == '{':
                    delimiterStack.append(nextChar)
                    arguments.append('')
                    if nextChar == '[': numDefaultArgs += 1
                else:
                    # Not starting a new argument, we're done
                    break
            elif nextChar == '[' or nextChar == '{':
                # Nested brackets/braces
                delimiterStack.append(nextChar)
                arguments[-1] += nextChar
            elif nextChar == ']' or nextChar == '}':
                # Check if we are closing a bracket or brace
                # Ensure this matches the last addition to the stack
                lastAddition = delimiterStack.pop()
                if (nextChar == ']' and lastAddition != '[') or (nextChar == '}' and lastAddition != '{'):
                    print("ERROR: Stack doesn't match when parsing macro \"{}\" for line \"{}\"".format(key,aline))
                    return aline
                if len(delimiterStack):
                    arguments[-1] += nextChar
            else:
                # We're appending more info to the argument
                arguments[-1] += nextChar
            
            # We processed a character
            # Store this information so we can replace the arguments later
            numArgChars += 1

        if len(arguments) == 0 and not self.hasDefaultArgs:
            print("ERROR: Failed to retrieve argument(s) associated with \"{}\" in line \"{}\"".format(self.keyString,aline))
            return aline

        # If there's just one argument and it's a standard argument and it's empty and default is supported, this is asking for all default arguments
        if len(arguments) == 1 and arguments[0] == '' and numDefaultArgs == 0 and self.hasDefaultArgs:
            arguments.pop()

        # We now have the argument(s)
        # Do some sanity checks
        if not self.hasDefaultArgs and numDefaultArgs != 0:
            print("ERROR: Provided default arguments to a macro which does not support default arguments ({})".format(key))
            return aline
        if len(arguments)-numDefaultArgs != self.numArgs-len(self.defaultArgs) or numDefaultArgs > len(self.defaultArgs):
            # Check if people just neglected to close their macro (example: \pt instead of \pt{} is a simplistic version of an encountered problem)
            if len(arguments)-numDefaultArgs-1 == self.numArgs-len(self.defaultArgs) and arguments[-1] == "":
                # Remove the worthless extra argument
                arguments.pop()
            else:
                print("ERROR: Specified {:d} standard arguments and {:d} default arguments, but the macro expected {:d} standard arguments and up to {:d} default arguments ({})".format(len(arguments)-numDefaultArgs,numDefaultArgs,self.numArgs-len(self.defaultArgs),len(self.defaultArgs),self.keyString))
                print('  Hint: {}'.format(aline.strip()))
                return aline

        # Add default argument(s) if not specified
        if self.hasDefaultArgs:
            for iArg in range(numDefaultArgs,len(self.defaultArgs)):
                arguments.insert(iArg,self.defaultArgs[iArg])

        # One more sanity check, everything should be equal now
        if len(arguments) != self.numArgs:
            print("ERROR: Something went wrong when forming argument-based macro, got {:d} arguments instead of {:d} for {}".format(len(arguments),self.numArgs,self.keyString))
            return aline

        # We're finally ready to make the substitutions
        result = self.valString
        for iSub in range(1,self.numArgs+1):
            #print iSub-1," : ",arguments[iSub-1]," + ",result
            #print "#%d"%(iSub)
            
            # Need to be careful with special command characters (such as \g)
            # Using re.escape() is too all-encompassing (escapes periods which we often want to substitute such as in |eta| < 4.5)
            escapedKey = ""
            escapedVal = ""
            for checkIndex in [0,1]:
                lastWasNewEsc = False
                inputStr = "#%d"%(iSub) if checkIndex == 0 else arguments[iSub-1]
                escapedStr = ""
                for aChar in inputStr:
                    if lastWasNewEsc:
                        if aChar == '\\':
                            lastWasNewEsc = False
                        else:
                            escapedStr += '\\'
                            lastWasNewEsc = False
                    elif aChar == '\\':
                        lastWasNewEsc = True
                    escapedStr += aChar
                if checkIndex == 0:
                    escapedKey = escapedStr
                else:
                    escapedVal = escapedStr
            result = re.sub(escapedKey,escapedVal,result)
                            
            # result = re.sub("#%d"%(iSub),arguments[iSub-1],result)
            #result = re.sub(re.escape("#%d"%(iSub)),re.escape(arguments[iSub-1]),result)
            #print result,"\n"

        # Note: macro substitutions can have multiple backslashes (should be two...)
        # If substituting a macro, we want to get rid of them all
        startIndex = keyResult.start()
        while startIndex > 0 and aline[startIndex-1] == '\\':
            startIndex -= 1
        if keyResult.start()-startIndex > 1:
            print("WARNING: Removing {:d} backslashes in likely sub-macro substitution, only two expected".format(keyResult.start()-startIndex()+1))
        
        toReturn = aline[0:startIndex]+result+aline[keyResult.start()+len(self.keyString)-1+numArgChars:]
        
        #print result
        #print aline[startIndex:keyResult.start()+len(self.keyString)-1+numArgChars]
        #print toReturn
        
        return toReturn

    def process_line(self,aline):
        # Skip the definition of the macro itself
        if self.macroDefNew.search(aline) or self.macroDefDRC.search(aline):
            return aline

        # Skip macro definitions which depend on this macro
        if self.macroSubDefNew.search(aline) or self.macroSubDefDRC.search(aline):
            return aline

        #print self.keyString, "\t", aline
        # Look for standard arguments first
        result = aline
        keyResult = re.search(self.keyString,result)
        previousResult = result
        while keyResult:
            result = self.process_line_helper(result,keyResult)
            keyResult = re.search(self.keyString,result)
            if result==previousResult:
                # Have not actually changed anything; bail out
                return result
            previousResult = result
        return result
        

def copyfile_list(original_file,original_extension,output_base):
    # Ensure the extension starts with a period
    if not original_extension.startswith("."):
        original_extension = '.'+original_extension
    escaped_original_extension = '\\'+original_extension

    # Get the list of matching file names with different extensions
    files = glob(re.sub(escaped_original_extension,'',original_file)+".*")

    # Ensure we found something
    if len(files) == 0:
        print("ERROR: failed to find any files matching: {}".format(original_file))
        return False
    
    # Conver this into a list of extensions
    original_base = re.sub(escaped_original_extension,'',original_file)
    file_extensions = [ re.sub(original_base,'',afile) for afile in files ]

    # Ensure that we found the original extension
    # The original is mandatory, the others are optional
    if not original_extension in file_extensions:
        print("ERROR: failed to find the requested file: {} {} {} {} {}".format(original_file,original_extension,file_extensions,files,original_base))
        return False

    # Copy files
    for extension in file_extensions:
        copyfile(original_base+extension,output_base+extension)

    return True












# Latex / PDFLatex Version check!
if tables:
    lines = subprocess.Popen(['pdflatex','--version'], stdout=subprocess.PIPE,stderr=subprocess.STDOUT,text=True).communicate()[0].strip().split('\n')
    for aline in lines:
      if 'pdfTeX' in aline and '3.141592-' in aline or 'Copyright 2007' in aline:
          print('Appears that you have a very old version of pdflatex running.  That will miss')
          print(' a number of features that are important for using tables.  I recommend that')
          print(' you upgrade to a newer version')
          # print(' you do the following before you run: ')
          # print('export PATH=/afs/cern.ch/sw/XML/texlive/latest/bin/x86_64-linux:$PATH')
          print('')
          print('')
          break

if not os.access(inputFile,os.R_OK):
    print('Could not locate input file {}'.format(inputFile))
    sys.exit(1)

if not os.access(outputFile,os.R_OK):
    if verbose: print('Creating directory {}'.format(outputFile))
    os.mkdir(outputFile)
if not outputFile.endswith('/'): outputFile += '/'

# Setup chapter numbering
chapter_no = ''
if chapters:
    chapter_no = 'ch00_'
    if hundred>2: print('WARNING: Using chapters and hundreds of figures -- are you sure you need this?')

# Function for helping search for files...
def search_file(filename, search_path, pathsep=os.pathsep):
    """ Given a search path, find file with requested name """
    for path in search_path.split(pathsep):
        candidate = os.path.join(path, filename)
        if os.path.exists(candidate): return os.path.abspath(candidate)
    return None

def clean_cap( cap,a,n,fig=True, labels=None):
    # Treat the label in a caption...
    if '\\label{' in cap:
        if cap.count('\\label{')>1:
            print('WARNING: Detected more than one label in caption for Fig {:d}; attempted to treat the first'.format(n))
        if '}' in cap.split('\\label{')[1]:
            cap = cap.split('\\label{')[0] + cap.split('\\label{')[1][ cap.split('\\label{')[1].find('}')+1:]
        ## Funny case - if the label follows the } in a caption, then we can get nasty formatting
        else:
            cap = cap.split('\\label{')[0]
    # Try to fix references
    if '\\ref{' in cap or '\\eqref' in cap:
        missing_refs = []
        for command, _, labelname in re.findall(r'(?P<command>(\\ref|\\eqref))\{(?P<labelname>.*?)\}', cap):
            old = "%s{%s}"%(command, labelname)
            new = ''
            if labels is not None and labelname in labels:
                new = labels[labelname]
            else:
                new = '(???)'
                missing_refs.append(labelname)
            cap = cap.replace(old, new)
            if verbose:
                print("in caption, replaced '{}' with '{}'".format(old, new))
        if missing_refs: print('WARNING: need to fix references in captions: {}'.format(str(missing_refs)))
    # Watch out for remaining references and citations
    if '\\ref{' in cap or '\\eqref' in cap:
        if fig: print('WARNING: Reference found in caption fig{}{:d}. You will have to fix this by hand.  Sorry!'.format(a,n))
        else:   print('WARNING: Reference found in caption tab{}{:d}. You will have to fix this by hand.  Sorry!'.format(a,n))
    if '\\cite{' in cap:
        if fig: print('WARNING: Citation found in caption fig{}{:d}%s. This should be reported - they should have been removed!'.format(a,n))
        else:   print('WARNING: Citation found in caption tab{}{:d}%s. This should be reported - they should have been removed!'.format(a,n))
    if '\\vspace{' in cap:
        if cap.count('\\vspace{')>1:
            print('You are crazy with more than one vspace per caption... you are going to kill me... treating the first in {} fig{:d}'.format(a,n))
        cap = cap.split('\\vspace{')[0] + cap.split('\\vspace{')[1][ cap.split('\\vspace{')[1].find('}')+1:]
    #if '\\bar{' in cap:
    #    print('Do not know if we use CSS, so cannot yet put an overbar on characters.  Sorry!  Please pick your own formatting for %s fig'%a,n)

    # Replace a bunch of characters for the html processing...
    cap_dict = [ # First clear out things that will screw with the html!
                { '<':'&lt;' , '>':'&gt;' , '\\{':'&#123;' , '\\}':'&#125;' , } ,
                 # Any ATLAS ones that have to go first...
                { '\\DeltaR':'&Delta;R<sub>cone</sub>' , '_R':'<sub>R</sub>' , '_\eta':'<sub>&eta;</sub>' ,
                  '_T':'<sub>T</sub>' , '_{R}':'<sub>R</sub>' , '_{T}':'<sub>T</sub>' ,
                  '^2':'<sup>2</sup>' , '^{2}':'<sup>2</sup>' ,  '\\ptjet':'p<sub>T</sub><sup>jet</sup>' ,
                  '\\rapjet':'y' , '\\antikt':'anti-k<sub>t</sub>' , '\\etajet':'&eta;' ,
                  '\\pttrkjet':'p<sub>T</sub><sup>track jet</sup>' , '\\etaDet':'&eta;<sub>det</sub>' ,
                  '\\insitu':'in situ' , '\\etjet':'\et<sup>jet</sup>' , '^{\\gamma}':'<sup>&gamma;</sup>' ,
                  '\\gammajet':'&gamma;-jet' , '\\Npv':'N<sub>PV</sub>' , '\\pythia':'PYTHIA' ,
                  '\\Antikt':'Anti-k<sub>t</sub>' , '\\herwigpp':'Herwig++' , '\\mathrm':'' , '\\textrm':'' ,
                  '\\operatorname':'' , '\\etaa':'&eta;' ,
                  '\\mh':'m<sub>H</sub>',
                  '\\hgg':'H&rarr;&gamma;&gamma;','\\hZZllll':'H&rarr;ZZ*&rarr;llll',
                  '\\hWWlnln':'H&rarr;WW*&rarr;l&nu;l&nu;','\\htt':'H&rarr;&tau;&tau;',
                  '\\Cc':'&kappa;','\\Rr':'&lambda;','\\BRinv':'BR<sub>inv.,undet.</sub>',
                  '\\ggF':'gg&rarr;H' , '\\langle':'&lang;' , '\\rangle':'&rang;' ,
                  '\\alphas':'&alpha;<sub>S</sub>' ,
                } ,
                 # Other physics particles that are pretty common
                { '\\squark':'q&#771;' , '\\gluino':'g&#771;' , '\\stop':'t&#771;' , '\\stopone':'t&#771;<sub>1</sub>' , '\\sbottom':'b&#771;' , '\\slepton':'&#8467;&#771;' ,
                  '\\ninoone':'&chi;&#771;<sub>1</sub><sup>0</sup>' , '\\chinoonepm':'&chi;&#771;<sub>1</sub><sup>&plusmn;</sup>' ,
                  '\\ninotwo':'&chi;&#771;<sub>2</sub><sup>0</sup>' , '\\chinotwopm':'&chi;&#771;<sub>2</sub><sup>&plusmn;</sup>' ,
                  '\\chinoonem':'&chi;&#771;<sub>1</sub><sup>-</sup>' , '\\chinoonep':'&chi;&#771;<sub>1</sub><sup>+</sup>' ,
                  '\\stopone':'t&#771;<sub>1</sub>' , '\\Zboson':'Z' , '\\Wboson':'W' , '\\Hboson':'H' , '\\\gravino':'G&#771;' ,
                  # In case they're unpacked
                  '\\tilde{q}':'q&#771;' , '\\tilde{g}':'g&#771;' , '\\tilde{t}':'t&#771;' , '\\tilde{b}':'b&#771;' , '\\tilde{\\chi}':'&chi;&#771;' ,
                  } ,
                 # Standard guys
                {'\\texttt':''           , '\\textsc':''       ,
                 '\\rm':''               , '\\alpha':'&alpha;' , '\\beta':'&beta;' ,
                 '\\gamma':'&gamma;'     , '\\delta':'&delta;' , '\\epsilon':'&epsilon;' ,
                 '\\zeta':'&zeta;'       , '\\eta':'&eta;'     , '\\theta':'&theta;' ,
                 '\\iota':'&iota;'       , '\\kappa':'&kappa;' , '\\lambda':'&lambda;' ,
                 '\\mu':'&mu;'           , '\\nu':'&nu;'       , '\\xi':'&xi;' ,
                 '\\omicron':'&omicron;' , '\\pi':'&pi;'       , '\\rho':'&rho;' ,
                 '\\sigma':'&sigma;'     , '\\tau':'&tau;'     , '\\upsilon':'&upsilon;' ,
                 '\\chi':'&chi;'         , '\\phi':'&phi;'     , '\\psi':'&psi;' ,
                 '\\omega':'&omega;'     , '\\Alpha':'&Alpha;' , '\\Beta':'&Beta;' ,
                 '\\Gamma':'&Gamma;'     , '\\Delta':'&Delta;' , '\\Epsilon':'&Epsilon;' ,
                 '\\Zeta':'&Zeta;'       , '\\Eta':'&Eta;'     , '\\Theta':'&Theta;' ,
                 '\\Iota':'&Iota;'       , '\\Kappa':'&Kappa;' , '\\Lambda':'&Lambda;' ,
                 '\\Mu':'&Mu;'           , '\\Nu':'&Nu;'       , '\\Xi':'&Xi;' ,
                 '\\Omicron':'&Omicron;' , '\\Pi':'&Pi;'       , '\\Rho':'&Rho;' ,
                 '\\Sigma':'&Sigma;'     , '\\Tau':'&Tau;'     , '\\Upsilon':'&Upsilon;' ,
                 '\\Chi':'&Chi;'         , '\\Phi':'&Phi;'     , '\\Psi':'&Psi;' ,
                 '\\Omega':'&Omega;'     , '\\rightarrow':'&rarr;' , '\\sqrt':'&radic;' ,
                 '\\sim':'&sim;'         , '\\mp':'&#8723;'    ,
                 '\\pm':'&plusmn;'       , '\\prod':'&prod;'   , '\\sum':'&sum;' ,
                 '\\int':'&int;'         , '\\equiv':'&equiv;' , '\\approx':'&approx;' ,
                 '\\geq':'&ge;'          , '\\leq':'&le;'      , '\\times':'&times;' , 
                 '\\neq':'&ne;'          , '\\protect':''      , '\\infty':'&infin;' , 
                 '\\dagger':'&dagger;'   , '\\prime':'&prime;' , '\\varepsilon':'&varepsilon;' ,
                 '\\varphi':'&varphi'    , '\\textless':'<'    , '\\textgreater':'>' ,
                 '\\cdot':'&middot;'     , '\\sin':'sin'       , '\\cos':'cos' } ,
                 # ATLAS additions
                { '\ie':'i.e.'            , '\eg':'e.g.'           , '\AKT':'anti-k<sub>t</sub>' , 
                 '\\pt':'p<sub>T</sub>'  , '\\pT':'p<sub>T</sub>' , '\\PT':'p<sub>T</sub>' ,    
                 '\\et':'E<sub>T</sub>'  , '\\eT':'E<sub>T</sub>' , '\\ET':'E<sub>T</sub>' ,
                 '\\HT':'H<sub>T</sub>'  , '\\met':'E<sub>T</sub><sup>miss</sup>' , '\\MET':'E<sub>T</sub><sup>miss</sup>' ,
                 '_{MC}':'<sub>MC</sub>' , '_{ MC }':'<sub>MC</sub>' , '_{ MC}':'<sub>MC</sub>' ,
                 '_{Data}':'<sub>Data</sub>' , '_{ Data }':'<sub>Data</sub>' , '_{ Data}':'<sub>Data</sub>' ,
                 '^{MC}':'<sup>MC</sup>' , '^{ MC }':'<sup>MC</sup>' , '^{ MC}':'<sup>MC</sup>' ,
                 '^{Data}':'<sup>Data</sup>' , '^{ Data }':'<sup>Data</sup>' , '^{ Data}':'<sup>Data</sup>' ,
                 '\\topo':'topo-cluster' , '\\Topo':'Topo-cluster' , 'k_t':'k<sub>t</sub>' ,
                 'k_{t}':'k<sub>t</sub>' , '\\le':'&le;'           , '\\meff':'m<sub>eff</sub>' ,
                 '\\PGg':'&gamma;',        '\\PW':'W'              , '\\PZ':'Z' ,       '\\Pg':'g',
                 '\\ell':'&#8467;',        '\\Ell':'&#8466'        , '\\ifb':'fb<sup>-1</sup>',
                 '\\mathcal{S}':'&Sscr;' ,
                 # Some more generators
                 '{\sc Pythia}':'PYTHIA' , '{\sc Herwig}':'HERWIG' , '{\sc Perugia}':'PERUGIA' ,
                 '{\sc Herwig++}':'HERWIG++', '{\sc Alpgen}':'ALPGEN' , '{\sc Pythia8}':'PYTHIA8' , 
                 '\\nlojetpp':'NLOJET++' , '{\sc Jimmy}':'JIMMY', '{\sc mc@nlo}':'MC@NLO',
                 '{\sc mcfm}':'MCFM',
                 # Only need to treat eV-like symbols if they would look funny otherwise
                 '\\ev':'eV'            , '\\kev':'keV'          , '\\mev':'MeV' ,
                 '\\gev':'GeV'           , '\\tev':'TeV'          , '\hat{m}':'m&#770;' ,
                 '\hat{p}':'p&#770;' } ,
                 # \ttbar \qqbar - Thanks Markus C for the recommendation
                { '\\ttbar':'tt&#772;',
                  '\\bbbar':'bb&#772;',
                  '\\qqbar':'qq&#772;',
                  # Some for people who don't know the new macros
                  '\\bar{t}':'t&#772;',
                  '\\bar{b}':'b&#772;',
                  '\\bar{q}':'q&#772;',
                  # Not yet generic, as I don't want to think about coding that right now
                  '\\overline{M}':'M&#772;',
                } ,
                 # Hepnames - very incomplete. Long names first
                { '\\PHiggs':'H' ,
                  '\\Ptop':'t', '\\APtop':'t&#773;' ,
                  '\\Pep':'e<sup>+</sup>' , '\\Pem':'e<sup>-</sup>' ,
                  '\\Pmup':'&mu;<sup>+</sup>' , '\\Pmuon':'&mu;<sup>-</sup>' , # strangely, there is no \Pmum
                  '\\Ptaup':'&tau;<sup>+</sup>' , '\\Ptaum':'&tau;<sup>-</sup>' ,
                  '\\Pnue':'&nu;<sub>e</sub>' , '\\APnue':'&nu;&#773;<sub>e</sub>' ,
                  '\\Pnum':'&nu;<sub>&mu;</sub>' , '\\APnum':'&nu;&#773;<sub>&mu;</sub>' ,
                  '\\Pnut':'&nu;<sub>&tau;</sub>' , '\\APnut':'&nu;&#773;<sub>&tau;</sub>' ,
                  '\\PZprime':'Z\'' ,
                  '\\APbottom':'b&#773;' , '\\Pbottom':'b' ,
                  '\\HepProcess':'' } ,
                 # Short names
                { '\\PH':'H' , '\\Pe':'e' , '\\Pmu':'&mu;' , '\\Ptau':'&tau;' } ,
                 # Last resorts
                { '\\maybeitslash':'/' , '\\vec':'' } ,
                { '\\T':'T' , '\\,':' ' , '\\small':'' , '\\center':'' , '\\;':' ' ,
                  '\\bar':'anti-', '\\tilde':'', 
                  '\\ninoone':'&chi;<sup>0</sup><sub>1</sub>',
                  '\\chinoonepm':'&chi;<sup>&plusmn;</sup><sub>1</sub>',
                  '\\to':'&rarr;','\\sc':'','\\ge':'&ge;',
                  '\\ra':'&rarr;' , # Shorthand from some analyses
                  # Formatting that I can't handle yet
                  '\\it':'','\\bf':'','\\textit':'','\\textbf':'','\\emph':'','\\text':'' ,
                  '\\ensuremath':'','\\xspace':'', '``':'"' , '\'\'':'"' , '\\boldsymbol':'' ,
                  '\\smash':'' , '\\mathbf':'' , '\\scalebox':'' , '\\,':' ' ,
                  '--':'&ndash;' , '---':'&mdash;' , '\\large':'' , '\\bfseries':'' ,
                  '\\mbox':'' , '\\maybebm':'' , '\\maybesf':'' ,  '\\maybeit':'' ,
                  '\\mathcal':'' ,
                  '\\kern':'' , '\\gls':'' , # Can't do glossaries yet :(
                  ' 0.1em':'' , # this one is completely desperate
                  '\\V':'V', '\\TeV':'TeV', '\\MeV':'MeV', '\\GeV':'GeV', # units, last gasp
                }
               ]
    for adict in cap_dict:
        for akey in adict:

            if isMacroMatch(cap,akey,False):
                cap = cap.replace( akey , adict[akey] )
    # Now clean the caption of any remaining symbols... HTML won't want to digest $$ and \ all over the place...
    spcl = [ '$' , '%' , '\\right' , '\\left' , '\\' ]
    for x in spcl:
        cap = cap.replace('\\'+x,'QQQQQQ').replace(x,'').replace('QQQQQQ','\\'+x)
    # Spaces as well...
    cap = cap.replace('~',' ')

    # Now try to treat subs and sups more carefully....
    subsupstack = []
    bracestack = []
    lastchar = ''
    newcap = ''
    encounteredFailure = False
    for c in cap:
        # Start a subscript
        if c == '_' and lastchar != '\\':
            newcap += '<sub>'
            subsupstack.append('_')
        # Start a superscript
        elif c == '^' and lastchar != '\\':
            newcap += '<sup>'
            subsupstack.append('^')
        # Start a block
        elif c == '{' and lastchar != '\\':
            if lastchar == '_' or lastchar == '^':
                bracestack.append(lastchar)
            else:
                bracestack.append('{')
        # End a block
        elif c == '}' and lastchar != '\\':
            if len(bracestack) == 0:
                print("Failure to understand caption due to unmatched \{\} in caption: {}".format(cap))
                print("Falling back on old method")
                encounteredFailure = True
                break
            
            closingGroup = bracestack.pop()
            if closingGroup == '{':
                # Nothing special
                pass
            elif closingGroup == '_':
                # Close a subscript
                newcap += '</sub>'
            elif closingGroup == '^':
                # Close a superscript
                newcap += '</sup>'
            else:
                # Unexpected
                print("Failure to understand caption due to unexpected character in bracestack: {}".format(closingGroup))
                print("Falling back on old method")
                encounteredFailure = True
                break

        # Check if we have a single-character subscript or superscript
        elif lastchar == '_' or lastchar == '^':
            if len(subsupstack) > 0:
                newcap += c + ('</sub>' if subsupstack.pop() == '_' else '</sup>')
        # Normal character scenario
        else:
            newcap += c
        lastchar = c

    if encounteredFailure:
        insubsup = ''
        intag = ''
        lastchar = ''
        for l in cap:
            # Handle the case of an extra sub
            if l=='_' and lastchar!='\\':
                if insubsup=='': intag=''
                insubsup += 'b'
                newcap += '<sub>'
            # Handle the case of an extra sup
            elif l=='^' and lastchar!='\\':
                if insubsup==0: intag=''
                insubsup += 'p'
                newcap += '<sup>'
            elif l=='{' and lastchar!='\\':
                if insubsup!='':
                    intag+=l
                    # If this is not a sub or sup then add the '{'... This is dicey
                    if intag.count('{')>len(insubsup): newcap += l
                else: newcap += l
            elif l=='}' and lastchar!='\\':
                if len(insubsup)>0:
                    if insubsup[-1]=='b': newcap+='</sub>'
                    if insubsup[-1]=='p': newcap+='</sup>'
                    insubsup = insubsup[:len(insubsup)-1]
                    intag = intag[:intag.rfind('{')]
                    # This is something I should improve later
                else: newcap+=l
            elif l==' ':
                if len(insubsup)>0 and intag.count('{')==intag.count('}'): # break from tag
                    while len(insubsup):
                        if insubsup[-1]=='b': newcap+='</sub>'
                        if insubsup[-1]=='p': newcap+='</sup>'
                        insubsup = insubsup[:len(insubsup)-1]
                else:
                    if len(insubsup)>0: intag+=l
                    newcap+=l
            else:
                newcap+=l
                if len(insubsup)>0: intag+=l
                if len(intag)==1:
                    if intag=='\\':
                        print('WARNING: {} figure {:d} has an unrecognized command not in \{\} to begin a subscript. Formatting may come out funny...'.format(a,n))
                    else:
                        while len(insubsup):
                            if insubsup[-1]=='b': newcap+='</sub>'
                            if insubsup[-1]=='p': newcap+='</sup>'
                            insubsup = insubsup[:len(insubsup)-1]
                        intag = ''
            lastchar = l

        while len(insubsup):
            if insubsup[-1]=='b': newcap+='</sub>'
            if insubsup[-1]=='p': newcap+='</sup>'
            insubsup = insubsup[:len(insubsup)-1]
    cap = newcap

    # anything that was totally cleaned
    new_cap = cap.replace('{}','').replace('{ }',' ').replace('{','').replace('}','')
    return new_cap

################### Method for writing files #####################
def writeFiles( cap = None , figs = None , output_raw = None , n = 0 , verbose = False , aux = False , hundred = 2 , sub_ref = [],
                labels=None):
    global multifilecopy, chapters, chapter_no
    if None==cap or None==figs:
        print('Give me an input please...')
        return False
    if None==output_raw:
        print('Give me an output location...')
        return False
    result = True

    output = output_raw+'/'+chapter_no
    # Deal with sub-references...
    if '\\subref' in cap:
        chunks = cap.split('\\subref')
        new_cap = ''
        if not cap.startswith('\\subref'): new_cap = chunks.pop(0)
        for a in chunks:
            lab_text = ''
            started_sub_ref = False
            for c in a:
                if '{'==c:
                    started_sub_ref = True
                    continue
                elif '}'==c:
                    break
                elif started_sub_ref:
                    lab_text+=c
            if not lab_text.strip() in sub_ref:
                print('WARNING: Caption for fig {:d} found a subref that I could not identify'.format(n))
                new_cap += '(X)'
            else:
                new_cap += '('+chr(97+sub_ref.index(lab_text.strip()))+')'
            new_cap += a[a.find('}')+1:]
        cap = new_cap

    a = ''
    if aux: a = 'aux'

    # Clean up the caption...
    cap = clean_cap( cap,a,n, labels=labels)

    # First case: only one figure
    if len(figs)==1:
        if verbose: print('Working on figure {}{:d}'.format(a,n))
        atext = open(output+'fig%s_%0*d.txt'%(a,hundred,n),'w')
        atext.write(cap.strip())
        atext.close()
        if '.eps' in figs[0]:
            if multifilecopy:
                copyfile_list(figs[0],'.eps',output+'fig%s_%0*d'%(a,hundred,n))
            else:
                copyfile(figs[0],output+'fig%s_%0*d.eps'%(a,hundred,n))
        elif '.pdf' in figs[0]:
            if multifilecopy:
                copyfile_list(figs[0],'.pdf',output+'fig%s_%0*d'%(a,hundred,n))
            else:
                copyfile(figs[0],output+'fig%s_%0*d.pdf'%(a,hundred,n))
        elif '.png' in figs[0]:
            if multifilecopy:
                copyfile_list(figs[0],'.png',output+'fig%s_%0*d'%(a,hundred,n))
            else:
                copyfile(figs[0],output+'fig%s_%0*d.png'%(a,hundred,n))
            if not chapters:
                print('WARNING: file {} is a png - this is fine if it is an event display; otherwise please use a .pdf or .eps file'.format(figs[0]))
        elif '.PNG' in figs[0]:
            if multifilecopy:
                copyfile_list(figs[0],'.PNG',output+'fig%s_%0*d'%(a,hundred,n))
            else:
                copyfile(figs[0],output+'fig%s_%0*d.PNG'%(a,hundred,n))
        elif '.jpg' in figs[0]:
            if multifilecopy:
                copyfile_list(figs[0],'.jpg',output+'fig%s_%0*d'%(a,hundred,n))
            else:
                copyfile(figs[0],output+'fig%s_%0*d.jpg'%(a,hundred,n))
            if not chapters:
                print('WARNING: file {} is a jpeg - this is fine if it you are working on an LoI or TDR; otherwise please use a .pdf or .eps file'.format(figs[0]))
        elif '.JPG' in figs[0]:
            if multifilecopy:
                copyfile_list(figs[0],'.JPG',output+'fig%s_%0*d'%(a,hundred,n))
            else:
                copyfile(figs[0],output+'fig%s_%0*d.JPG'%(a,hundred,n))
            if not chapters:
                print('WARNING: file {} is a jpeg - this is fine if it you are working on an LoI or TDR; otherwise please use a .pdf or .eps file'.format(figs[0]))
        else:
            print('WARNING: Did not recognize figure type for {}'.format(figs[0]))
            print('Must be one of [.eps,.pdf,.png,.jpg]')
            result = False
    # Second case: multi-figure
    else:
        for i in range(len(figs)):
            if verbose: print('Working on {} figure {:d} subfigure {:d}'.format(a,n,i))
            atext = open(output+'fig%s_%0*d%s.txt'%(a,hundred,n,string.ascii_lowercase[i]),'w')
            atext.write(cap.strip())
            atext.close()

            if '.eps' in figs[i]:
                if multifilecopy:
                    copyfile_list(figs[i],'.eps',output+'fig%s_%0*d%s'%(a,hundred,n,string.ascii_lowercase[i]))
                else:
                    copyfile(figs[i],output+'fig%s_%0*d%s.eps'%(a,hundred,n,string.ascii_lowercase[i]))
            elif '.pdf' in figs[i]:
                if multifilecopy:
                    copyfile_list(figs[i],'.pdf',output+'fig%s_%0*d%s'%(a,hundred,n,string.ascii_lowercase[i]))
                else:
                    copyfile(figs[i],output+'fig%s_%0*d%s.pdf'%(a,hundred,n,string.ascii_lowercase[i]))
            elif '.png' in figs[i]:
                if multifilecopy:
                    copyfile_list(figs[i],'.png',output+'fig%s_%0*d%s'%(a,hundred,n,string.ascii_lowercase[i]))
                else:
                    copyfile(figs[i],output+'fig%s_%0*d%s.png'%(a,hundred,n,string.ascii_lowercase[i]))
                if not chapters: 
                    print('WARNING: file {} is a png - this is fine if it is an event display; otherwise please use a .pdf or .eps file'.format(figs[i]))
            elif '.PNG' in figs[i]:
                if multifilecopy:
                    copyfile_list(figs[i],'.PNG',output+'fig%s_%0*d%s'%(a,hundred,n,string.ascii_lowercase[i]))
                else:
                    copyfile(figs[i],output+'fig%s_%0*d%s.PNG'%(a,hundred,n,string.ascii_lowercase[i]))
            elif '.jpg' in figs[i]:
                if multifilecopy:
                    copyfile_list(figs[i],'.jpg',output+'fig%s_%0*d%s'%(a,hundred,n,string.ascii_lowercase[i]))
                else:
                    copyfile(figs[i],output+'fig%s_%0*d%s.jpg'%(a,hundred,n,string.ascii_lowercase[i]))
                if not chapters:
                    print('WARNING: file {} is a jpg - this is fine if it you are working on an LoI or TDR; otherwise please use a .pdf or .eps file'.format(figs[i]))
            elif '.JPG' in figs[i]:
                if multifilecopy:
                    copyfile_list(figs[i],'.JPG',output+'fig%s_%0*d%s'%(a,hundred,n,string.ascii_lowercase[i]))
                else:
                    copyfile(figs[i],output+'fig%s_%0*d%s.JPG'%(a,hundred,n,string.ascii_lowercase[i]))
                if not chapters:
                    print('WARNING: file {} is a jpg - this is fine if it you are working on an LoI or TDR; otherwise please use a .pdf or .eps file'.format(figs[i]))
            else:
                print('WARNING: Did not recognize figure type for {}'.format(figs[i]))
                print('Must be one of [.eps,.pdf,.png,.jpg]')
                result = False
    return result

### Function to make a table as a PDF and plunk it into the output directory
def make_table( tab_tex = None , output = None , n = 0 , aux = False , hundred = 2 , inc_cap = False , has_atlph = None , landscape = False , bonus = [] , verbose = False ):
    global chapter_no
    if tab_tex is None or ''==tab_tex:
        print('Found no table tex... something went wrong')
        return
    if output is None:
        print('Found no output directory for tables?')
        return

    # Watch for double backslashes from macro substitution
    # We expect some double backslashes for LATEX EOL
    # As such, we expect some triple backslashes (EOL followed by macro)
    # However, double backslashes followed by non-backslash is not expected
    # Can't get the negative look-behind assertion to work, hence the below
    tab_tex = tab_tex.replace('\\\\[','ZZZZZZZZZZZQQ')
    tab_tex = tab_tex.replace('\\\\%','ZZZZZZZZZZZCC')
    tab_tex = tab_tex.replace('\\\\$','ZZZZZZZZZZZDD')
    tab_tex = re.sub('QQQQQ',r'\\\\\\',re.sub(r'\\(\\\S)',r'\1',re.sub(r'\\\\\\','QQQQQ',tab_tex)))
    tab_tex = tab_tex.replace('ZZZZZZZZZZZCC','\\\\%')
    tab_tex = tab_tex.replace('ZZZZZZZZZZZQQ','\\\\[')
    tab_tex = tab_tex.replace('ZZZZZZZZZZZDD','\\\\$')

    # Handling a couple of special cases for the TDAQ TDR
    tab_tex = tab_tex.replace('\\{[}GeV]','[$\\GeV$]').replace('\\{[}kHz]','[$\\kHz$]')

    # This loop is largely extracted from the main text.  It could probably be done
    #  in a way that's more consistent with the figures, but this is pretty convenient,
    #  since we needed the tex anyway
    thecaption = ''
    midcaption = False
    for aline in tab_tex.split('\n'): # These lines are already cleaned

        # Case: starting the caption
        # @TODO: make this a bit more clever - should recognize certain allowed characters after caption or icaption and not start on a command like '\captionblahblahblah'
        if '\\caption' in aline or '\\icaption' in aline:
            if verbose: print('Beginning caption for table {:d}'.format(n))
            if '\\icaption' in aline: capline = aline.split('\\icaption')[1]
            elif '\\caption' in aline: capline = aline.split('\\caption')[1]
            if verbose: print('Examining caption {}'.format(capline))
            # Special case: try to handle 'short' captions
            if capline.find('[')>=0 and capline.find(']')>=0 and capline.find('{')>capline.find('['):
                capline = capline[capline.find(']')+1:]    
            # Special case: ignore an empty caption{} command which may be that of a sub-figure
            if capline.rstrip().replace(' ','') == '{}':
                if verbose: print('Ignoring empty caption assuming it wasn\'t intentional')
                continue
            if verbose: print('Found to be non-empty')
            left = capline.count('{')
            right = capline.count('}')
            # Two cases: entire caption on one line, or caption broken into several lines...
            #   Second part handles the case of a caption command followed by the caption on the next line
            if left>right or len(capline.strip())<1:
                midcaption=True
                thecaption = capline[1:].strip()+' '
                if verbose:
                    print('Moving on with caption: {}'.format(thecaption))
                    print('With l,r {} {} From {}'.format(left,right,capline))
                    print('Beginning from {}'.format(aline))
                continue
            elif left==right: # Nicely formatted, thank you
                thecaption = capline[1:capline.rfind('}')]
            else:
                thecaption = capline[1:capline.rfind('}')]
                left = thecaption.count('{')
                right = thecaption.count('}')
                while left<right:
                    thecaption = capline[:capline.rfind('}')]
                    right = thecaption.count('}')
                # Done finding that... bit of a trick, huh?

        # Case: mid-caption
        if midcaption:
            if verbose: print('Still inside a caption {}'.format(thecaption))
            left = left + aline.count('{')
            right = right + aline.count('}')
            if left>right: # Still going!
                thecaption += aline.strip()+' '
                if verbose: print('Continuing to work on caption {}'.format(thecaption))
                continue
            elif left==right: # Nicely formated, thanks!
                if verbose:
                    print('Found nice formatting. Cleaning line: {} to add to caption {}'.format(aline,thecaption))
                thecaption += aline.strip()[:aline.strip().rfind('}')]
                midcaption = False
                if verbose:
                    print('Done working on caption {} from line {}'.format(thecaption,aline))
            else:
                # Now we're done before the end of the line...
                thecaption += aline[:capline.rfind('}')]
                if verbose:
                    print('Will try to trim caption {}'.format(thecaption))
                    print('By at least {:d} cases of \}'.format(right-left))
                left = thecaption.count('{')
                right = thecaption.count('}')
                while left<right:
                    thecaption = capline[:capline.rfind('}')]
                    right = thecaption.count('}')
                midcaption = False
                if verbose: print('Done with caption {}'.format(thecaption))
                # Done finding that... bit of a trick, huh?

    # Clean up the caption...
    a = ''
    if aux: a = 'aux'
    cap = clean_cap( thecaption,a,n,False, labels)

    if verbose: print('Working on table {}{:d}'.format(a,n))
    atext = open(output+'/'+chapter_no+'tab%s_%0*d.txt'%(a,hundred,n),'w')
    atext.write(cap.strip())
    atext.close()

    # Make a directory for tex work
    if not os.access(output,os.R_OK):
        print('Could not find output directory')
        sys.exit(1)
    if os.access(output+'table_tmp',os.R_OK):
        print('Found old temp table directory - something must have gone wrong!  Cleaning up...')
        shutil.rmtree( output+'table_tmp' )
    os.mkdir(output+'table_tmp')

    # Deal with any figures that are in the table - soft links
    if '\\includegraphics' in tab_tex:
        if verbose: print('Dealing with figures in table')
        for prefig_path in tab_tex.split('\\includegraphics'):
            fig_path = prefig_path[ prefig_path.find('{')+1:prefig_path.find('}') ]
            final_path = findFigure(fig_path,os.getcwd(),'',None,verbose)
            os.symlink( final_path , output+'table_tmp/'+fig_path.split('/')[-1] )
            tab_tex = tab_tex.replace( fig_path , fig_path.split('/')[-1] )
        # Done with all figures
    # Dealt with figures

    # Now we make a dummy tex file, pdftex it, and put it in place
    atex = open(output+'table_tmp/table.tex','w')
    atex.write("""% Dummy tex table template
\\documentclass""")
    if landscape: atex.write('[landscape]')
    atex.write("""{article}
\\usepackage{caption}
\\usepackage{amsmath}
\\usepackage{a4wide}
\\usepackage{graphicx}
\\usepackage{rotating}
\\usepackage{longtable}
\\usepackage{multirow}
\\usepackage[usenames,dvipsnames,table]{xcolor}
\\usepackage{siunitx}
\\usepackage{upgreek}
\\usepackage{booktabs}
\\usepackage{dcolumn}% Align table columns on decimal point
""")
    if '\\begin{tabularx}{\\textwidth}' in tab_tex or '\\begin{tabular*}{\\textwidth}' in tab_tex:
        atex.write('\\usepackage[a4paper]{geometry} % tabularx; assume people are using sensible page sizes\n')
    else:
        atex.write('\\usepackage[a0paper]{geometry} % HUGE pages, we are going to crop them anyway\n')
    if '\\specialrule' in tab_tex or '\\toprule' in tab_tex or '\\midrule' in tab_tex or '\\bottomrule' in tab_tex or '\\cmidrule' in tab_tex:
        atex.write('\\usepackage{booktabs}\n')
    if '\\ldelim' in tab_tex:
        atex.write('\\usepackage{bigdelim}\n')
    if 'dashrule' in tab_tex:
        atex.write('\\usepackage{dashrule}\n')
    if 'xspace' in tab_tex:
        atex.write('\\usepackage{xspace}\n')
    if '\\bigstrut' in tab_tex:
        atex.write('\\usepackage{bigstrut}\n')
    if '\\hhline' in tab_tex:
        atex.write('\\usepackage{hhline}\n')
    if '\\checkmark' in tab_tex or '\\geqslant' in tab_tex:
        atex.write('\\usepackage{amssymb}\n')
    if '\\cdashline' in tab_tex:
        atex.write('\\usepackage{arydshln}\n')
    if '\\bm{' in tab_tex:
        atex.write('\\usepackage{bm}\n')
    if '\\slashed' in tab_tex:
        atex.write('\\usepackage{slashed}\n')
    if '\\SI{' in tab_tex or '\\sisetup{' in tab_tex or '\\num{' in tab_tex or 'table-format' in tab_tex or 'table-number-alignment' in tab_tex or 'round-mode' in tab_tex: #Looks like SI package is in use
        atex.write('\\usepackage{siunitx}\n')
    if '\\num' in tab_tex: # This also looks like siunitx commands...
        if verbose: print('Adding siunitx package - not totally clear, but I do see \\num in there')
        atex.write('\\usepackage{siunitx}\n')
    if '\\begin{xtabular}' in tab_tex:
        atex.write('\\usepackage{xtab}\n')
    if '\\tymin' in tab_tex:
        atex.write('\\usepackage{tabulary}\n')
    if '\\rowcolor' in tab_tex:
        atex.write('\\usepackage{colortbl}\n')
    if '\\diagbox' in tab_tex:
        atex.write('\\usepackage{diagbox}\n')
    if '\\begin{tabularx}' in tab_tex:
        atex.write('\\usepackage{tabularx}\n')
    if '\\ding' in tab_tex:
        atex.write('\\usepackage{pifont}\n')
    if '\\rowcolor' in tab_tex:
        atex.write('\\usepackage{color, colortbl}\n')
        atex.write('\\definecolor{Gray}{gray}{0.9}\n')
    if '\\mathfrak' in tab_tex:
        atex.write('\\usepackage{amsfonts}\n')
    if '\\acrshort' in tab_tex:
        print('WARNING: I don\'t yet know how to deal with glossaries.  Will fake it')
        atex.write('\\newcommand{\\acrshort}[1]{#1}\n')
    if '\\gls' in tab_tex:
        print('WARNING: I don\'t yet know how to deal with glossaries.  Will fake it')
        atex.write('\\newcommand{\\gls}[1]{#1}\n')
        atex.write('\\newcommand{\\glspl}[1]{#1}\n')
    if 'adjustbox' in tab_tex:
        atex.write('\\usepackage{adjustbox}\n')
    if 'tabularew' in tab_tex:
        atex.write('\\usepackage{tabularew}\n')
    if 'adjustbox' in tab_tex:
        atex.write('\\usepackage{adjustbox}\n')
    if '\\muup' in tab_tex:
        atex.write('\\usepackage{txfonts}\n')
    if 'longtable' in tab_tex:
        atex.write('\\usepackage{longtable}\n')
    if '\\widthof' in tab_tex:
        atex.write('\\usepackage{calc}\n')
    if '\\subfloat' in tab_tex:
        atex.write('\\usepackage{subfig}\n')
    if '\\HepProcess' in tab_tex:
        atex.write('\\usepackage[italic]{hepparticles}\n')
    if '\\href' in tab_tex:
        atex.write('\\usepackage{hyperref}\n')
    if 'threeparttable' in tab_tex or 'ThreePartTable' in tab_tex:
        atex.write('\\usepackage{threeparttable}\n')
        atex.write('\\usepackage{threeparttablex}\n')
    if '\\makecell' in tab_tex:
        atex.write('\\usepackage{makecell}\n')
    # Too many cases to deal with, just hope they have it on their system...
    atex.write('\\usepackage[italic]{hepnames}\n')
    had_latex_path = False
    latex_path = "latex/"
    if len(bonus)>0:
        #print('Attempting to add in difficult macro lines to table...')
        for b in bonus:
            if 'ATLASLATEXPATH' in b:
                had_latex_path=True
                the_cwd = os.getcwd()
                # Make this an absolute path
                latex_path = b.split("ATLASLATEXPATH")[1].replace("}","").replace("{","").rstrip()
                latex_path = "/".join([the_cwd,latex_path])
                if latex_path!='':
                    atex.write('\n\\newcommand*{\\ATLASLATEXPATH}{%s}\n'%latex_path)
                else:
                    had_latex_path=False
                print('Attempted to use {} as ATLASLATEXPATH: {}'.format(latex_path,had_latex_path))
    if has_atlph is not None: 
        atex.write("\\usepackage{xspace}\n")
        # Check for the new fancy directory structure
        if os.access( 'latex' , os.R_OK ) and os.access( 'latex/atlasphysics.sty' , os.R_OK ):
            if not had_latex_path: atex.write('\\newcommand{\\ATLASLATEXPATH}{latex/}\n')
            atex.write("\\usepackage%s{latex/atlasphysics}\n"%('' if has_atlph is '' else '['+has_atlph+']'))
        else:
            if not had_latex_path: atex.write('\\newcommand{\\ATLASLATEXPATH}{.}\n')
            atex.write("\\usepackage%s{\\ATLASLATEXPATH/atlasphysics}\n"%('' if has_atlph is '' else '['+has_atlph+']'))
    # Add in bonus lines if necessary
    if len(bonus)>0:
        #print('Attempting to add in difficult macro lines to table...')
        for b in bonus:
            if not 'ATLASLATEXPATH' in b: atex.write('\n'+b+'\n')

    atex.write("""%
\\newcommand{\\eatme}[2][]{}
\\pagestyle{empty} % Turn off page numbers
\\begin{document}
\\begin{minipage}[center]{1.12\\textwidth}
""")
    # Bunch of extra substitutions
    cleaned_tex = tab_tex.replace('{table*}','{table}').replace('\\begin{table}','').replace('\\end{table}','')
    if not inc_cap: cleaned_tex = cleaned_tex.replace('\\captionsetup','\\eatme').replace('\\caption','\\eatme').replace('\\icaption','\\eatme')
    else: cleaned_tex = cleaned_tex.replace('\\caption','\\captionof{table}').replace('\\icaption','\\captionof{table}')
    cleaned_tex = cleaned_tex.replace('\\begin{center}','').replace('\\end{center}','')
    final_tex = ''
    for aline in cleaned_tex.split('\n'):
        if '\\newcommand' in aline: final_tex += aline.split('\\newcommand')[0]+'\n'
        else:                       final_tex += aline                         +'\n'
    atex.write( final_tex )
    atex.write("""
% End of table
\\end{minipage}
\\end{document}
""")
    atex.close()

    # Go there to run pdflatex
    the_cwd = os.getcwd()

    # Take the style file(s) with us if necessary
    if has_atlph is not None:
       # Check for the directory
       if verbose: print("Looking for atlasphysics.sty in {}".format(latex_path))
       if os.access( latex_path , os.R_OK ) and os.access( latex_path+'/atlasphysics.sty' , os.R_OK ): os.symlink( latex_path , output+'table_tmp/latex' )
       # Check for the file alone
       elif os.access('atlasphysics.sty',os.R_OK):
           for asty in glob('atlas*.sty'): copyfile(asty,output+'table_tmp/'+asty)
       # Get a "safe" version
       elif os.access('/afs/cern.ch/user/z/zmarshal/public/tex/atlasphysics.sty',os.R_OK):
           print('Could not find your local version of atlasphysics - using my own...')
           copyfile('/afs/cern.ch/user/z/zmarshal/public/tex/atlasphysics.sty',output+'table_tmp/atlasphysics.sty')
    for astyfile in glob('*sty'): # Overkill, but helps sometimes
        copyfile(astyfile,output+'table_tmp/'+astyfile)
    os.chdir( output+'table_tmp/' )

    if verbose:
        print('Running pdflatex!')
        tex_it = subprocess.Popen(['pdflatex','table.tex'])
        x = tex_it.wait()
        tex_it = subprocess.Popen(['pdflatex','table.tex'])
        x = tex_it.wait()
    else:
        tex_it = subprocess.Popen(['pdflatex','table.tex'],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        x = tex_it.wait()
        tex_it = subprocess.Popen(['pdflatex','table.tex'],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        x = tex_it.wait()

    if not os.access('table.pdf',os.R_OK):
        print('Table-making didn\'t work for some reason?')
        sys.exit(1)

    # Go back!
    os.chdir( the_cwd )
    # Move the new PDF into place and clean up
    if tabletex: shutil.copy(output+'table_tmp/table.tex',output+'/'+chapter_no+'tab%s_%0*d.tex'%(a,hundred,n))
    crop_table = subprocess.Popen(['pdfcrop',output+'table_tmp/table.pdf',output+'/'+chapter_no+'tab%s_%0*d.pdf'%(a,hundred,n)],\
                                  stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    crop_table.wait()
    shutil.rmtree( output+'table_tmp' )

    return True

##############################################################
# Get a specific page of a PDF, and return the new file name #
def PDFPage( figure , pageN ):
    #grab_page = subprocess.Popen(['pdftk',figure,'cat',pageN,'output',figure.replace('.pdf','_page'+pageN+'.pdf')],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    grab_page = subprocess.Popen(['gs','-sDEVICE=pdfwrite','-dNOPAUSE','-dBATCH','-dSAFER','-dFirstPage='+pageN,'-dLastPage='+pageN,'-sOutputFile='+figure.replace('.pdf','_page'+pageN+'.pdf'),figure],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    grab_page.wait()
    return figure.replace('.pdf','_page'+pageN+'.pdf')


################ Method for locating a figure ###################
def findFigure( afig = None , adir = None , bdir = None , pageN = None , verbose = False , recursing = False):
    if None==afig or None==adir:
        print('Give me input!! {} {} {}'.format(afig,adir,bdir))
        return afig

    # Recurse!
    if ' ' in adir:
        for a_adir in adir.split():
            test = findFigure( afig , a_adir , bdir , pageN , verbose , True )
            if os.access( test , os.R_OK ):
                return test
    if ' ' in bdir:
        for a_bdir in bdir.split():
            test = findFigure( afig , adir , a_bdir , pageN , verbose , True )
            if os.access( test , os.R_OK ):
                return test

    # First: take the path of the file that we were working from, and append the figure name
    if adir=='': adir='.'
    thefig = adir+'/'+afig
    if bdir is not None: thefig = adir+'/'+bdir+'/'+afig

    if verbose: print('Trying to find a figure from {} {} {} in {}'.format(adir,bdir,afig,thefig))

    # Before anything else: assume afig is the name of the figure and bdir is a search path separated by ' '
    if verbose: print('Looking for figure {} in {} using search path {}'.format(afig,adir,bdir))
    thefig = search_file(afig, bdir, ' ')

    if thefig!=None and os.access( thefig, os.R_OK):
        if verbose: print('Found figure {}'.format(thefig))
        thefig = thefig.replace('//','/')
        return thefig
    elif thefig==None: # Reset to previous value
        thefig = adir+'/'+afig
        if bdir is not None: thefig = adir+'/'+bdir+'/'+afig
    if thefig is not None: thefig = thefig.replace('//','/')

    # Easy case : the file name was given
    if os.access( thefig , os.R_OK ):
        if pageN is not None and '.pdf' in thefig: return PDFPage( thefig , pageN )
        return thefig

    # Second case : we have to figure out where the file is - first guess the two easy names
    if os.access( thefig+'.eps' , os.R_OK ):
        return thefig+'.eps'
    if os.access( thefig+'.pdf' , os.R_OK ):
        if pageN is not None: return PDFPage( thefig+'.pdf' , pageN )
        return thefig+'.pdf'
    if os.access( thefig+'.png' , os.R_OK ):
        return thefig+'.png'
    if os.access( thefig+'.jpg' , os.R_OK ):
        return thefig+'.jpg'

    # Third case: They want us to ignore the graphics path
    thefig = adir+'/'+afig
    if os.access( thefig+'.eps' , os.R_OK ):
        return thefig+'.eps'
    if os.access( thefig+'.pdf' , os.R_OK ):
        if pageN is not None: return PDFPage( thefig+'.pdf' , pageN )
        return thefig+'.pdf'
    if os.access( thefig+'.png' , os.R_OK ):
        return thefig+'.png'
    if os.access( thefig+'.jpg' , os.R_OK ):
        return thefig+'.jpg'

    # Fourth case: really hard now...
    for af in glob( thefig+'*' ):
        if af.endswith('.eps') or af.endswith('.pdf'):
            return af

    # Fifth case: fine, we take the png if it is there...
    for af in glob( thefig+'*' ):
        if af.endswith('.png'):
            return af

    # Sixth case: fine, we take the jpg if it is there...
    for af in glob( thefig+'*' ):
        if af.endswith('.jpg'):
            return af

    # Seventh case: if requested, go for the hard one
    global findall
    if findall:
        try:
            paths = [line[2:] for line in subprocess.check_output("find . -iname '*"+thefig.split('/')[-1]+"*'", shell=True).splitlines()]
            if len(paths)>1:
                print('WARNING: found multiple instances of {}:{}  Returning the first'.format(thefig,paths))
            if len(paths)>0:
                return paths[0]
        except:
            print('WARNING: to use find option, you must have python 3.1 or newer set up!')
            findall=False

    # Give up
    if not recursing: print('WARNING: Could not find any file called {} relative to directory {}'.format(afig,adir))
    return afig

def extractMacros(wholedoc, verbose):
    # let's try to handle the macros...

    # simple macros
    ptn_def  = re.compile("\\\\def\ *(\\\\.*?)\{(.*)\}")
    ptn_ren  = re.compile("\\\\renewcommand\{(\\\\.*?)\}\s*\{(.*)\}")
    ptn_new  = re.compile("\\\\newcommand{(\\\\.*?)\}\s*\{(.*)\}")
    ptn_news = re.compile("\\\\newcommand\*{(\\\\.*?)\}\s*\{(.*)\}")
    ptn_dec  = re.compile("\\\\DeclareRobustCommand{(\\\\.*?)\}\s*\{(.*)\}")

    # macros with arguments (and possibly default arguments)
    ptn_renA    = re.compile("\\\\renewcommand\{(\\\\.*?)\}\s*(\[[0-9]\])\s*\{(.*)\}")
    ptn_renAD   = re.compile("\\\\renewcommand\{(\\\\.*?)\}\s*(\[[0-9]\]\[[^\\[\\]{}]*\])\s*\{(.*)\}")
    ptn_newA    = re.compile("\\\\newcommand{(\\\\.*?)\}\s*(\[[0-9]\])\s*\{(.*)\}")
    ptn_newAD   = re.compile("\\\\newcommand{(\\\\.*?)\}\s*(\[[0-9]\]\[[^\\[\\]{}]*\])\s*\{(.*)\}")
    ptn_newsA   = re.compile("\\\\newcommand\*{(\\\\.*?)\}\s*(\[[0-9]\])\s*\{(.*)\}")
    ptn_newsAD  = re.compile("\\\\newcommand\*{(\\\\.*?)\}\s*(\[[0-9]\]\[[^\\[\\]{}]*\])\s*\{(.*)\}")
    ptn_decA    = re.compile("\\\\DeclareRobustCommand{(\\\\.*?)\}\s*(\[[0-9]\])\s*\{(.*)\}")
    ptn_decAD   = re.compile("\\\\DeclareRobustCommand{(\\\\.*?)\}\s*(\[[0-9]\]\[[^\\[\\]{}]*\])\s*\{(.*)\}")

    macro_dict = {}
    bonus = []

    # This will be a bit slower, but it will allow us to treat comments properly...
    long_command = ''
    for theline in wholedoc.split('\n'):

        # Comments, but not escaped % marks!
        aline = theline.replace('\\%','QQQQQQ').split('%')[0].replace('QQQQQQ','\\%')
        if '\\APHY@atlasdir' in aline: continue

        # Normal macros
        for k,v in ptn_def.findall(aline) + ptn_ren.findall(aline) + ptn_new.findall(aline) + ptn_dec.findall(aline) + ptn_news.findall(aline):
    
            ## get rid of ensuremath and xspace:
            #if re.search("\\\\ensuremath", v):
            #    v = re.sub("\\\\ensuremath\{(.*)\}", lambda x: "%s" % x.groups(), v)
    
            #if re.search("\\\\xspace", v):
            #    v = re.sub("\\\\xspace", " ", v)
            if k in ['\\la','\\Bgl']: continue # Nobody uses it, and it messes with label making
            if k is '\\editnote': continue # Shouldn't be there in a final draft, and hard to parse
            if aline.startswith('\\DeclareRobustCommand') and '\\def' in aline and not '\\def' in v:
                continue # Allowed to use \def in a robust command, but need to not try to parse the def
            if k.strip()=='\\e': continue
            if k.strip()=='\\m': continue
            if k.strip()=='\\arraystretch': continue # Don't redefine this - it's a number, basically...
            if k.strip()=='\\tabcolsep': continue # Don't redefine this - it's a magic number as well
            if k.strip()=='\\DefinitionHead': continue # Special for the TDAQ TDR -- it's in a table, skip it
            if '\\' in v: v = replaceMacros(macro_dict, v, verbose)
            macro_dict[k.strip().replace("\\","\\\\")] = v.replace("\\","\\\\")

        # Macros with standard arguments
        for key,arg,val in ptn_renA.findall(aline) + ptn_newA.findall(aline) + ptn_decA.findall(aline) + ptn_newsA.findall(aline):
            if key in ['\\editnote','\\Bgl']: continue # Shouldn't be there in a final draft, and hard to parse

            # There should be one [ and one ]
            if arg.count('[') != 1 or arg.count(']') != 1:
                print("WARNING: ignoring macro with malformed argument string: {} {}".format(key,arg))
                continue

            macro_dict[key.strip().replace("\\","\\\\")] = macro_with_args(key.replace("\\","\\\\"),arg,val.replace("\\","\\\\"))

        # Macros with optional arguments
        for key,arg,val in ptn_renAD.findall(aline) + ptn_newAD.findall(aline) + ptn_decAD.findall(aline) + ptn_newsAD.findall(aline):
            if key is '\\editnote': continue # Shouldn't be there in a final draft, and hard to parse

            # There should be an equal number of [ and ]
            if arg.count('[') != arg.count(']'):
                print("WARNING: ignoring macro with unmatched argument string (only single-line support currently provided): {} {}".format(key,arg))
                continue
            macro_dict[key.strip().replace("\\","\\\\")] = macro_with_args(key.replace("\\","\\\\"),arg,val.replace("\\","\\\\"))

        # Last one by hand... - this is for you guys who don't like brackets
        if '\\newcommand\\' in aline and not '[' in aline.split('{'):
            left = aline.replace('\\newcommand','').strip()
            k = left[:left.find('{')].strip()
            v = left[left.find('{')+1:left.rfind('}')]
            if '\\editnote' in k: continue
            ## get rid of ensuremath and xspace:
            #if re.search("\\\\ensuremath", v):
            #    v = re.sub("\\\\ensuremath\{(.*)\}", lambda x: "%s" % x.groups(), v)

            #if re.search("\\\\xspace", v):
            #    v = re.sub("\\\\xspace", " ", v)

            if not k.strip().replace("\\","\\\\") in macro_dict: macro_dict[k.replace("\\","\\\\")] = v.replace("\\","\\\\")

        # For now largely assuming that the hardest commands will be spread out over several lines
        if long_command=='' and '\\newcommand' in aline and aline.count('{') > aline.count('}'):
            long_command += aline+'\n'
        elif long_command!='':
            # Clean ending
            long_command += aline+'\n'
            if long_command.count('{') > long_command.count('}'): continue
            while long_command.count('{') < long_command.count('}'):
                long_command = long_command[:long_command.rfind('}')]
            if long_command.count('{') != long_command.count('}'):
                print('ERROR - could not parse command')
                print(long_command)
            bonus += [ long_command ]
            long_command = ''

        bonused = False
        if long_command=='':
            if '\\mathchardef' in aline or '\\newcommand*' in aline: # take the whole thing
                bonus += [ aline+'\n' ]
                bonused = True
            # Try to get multi-item commands - one explicit case to watch a {3} that isn't a multi-item macro from the TDAQ TDR
            if '\\newcommand' in aline and ('{2}' in aline or '{3}' in aline or '{4}' in aline) and not bonused and not '\\fthree' in aline:
                bonus += [ aline+'\n' ]
                bonused = True
            if '\\newcolumntype' in aline and not bonused:
                bonus += [ aline+'\n' ]
                bonused = True
            if '\\newacronym' in aline and not bonused:
                bonus += [ aline+'\n' ]
                bonused = True

    if verbose:
        for k,v in macro_dict.items():
            print("{}\t-->\t{}".format(k,v))
    return macro_dict,bonus

def isMacroMatch(aline,key,regexpmatch=True):
    global failed_keys
    if regexpmatch:
        result = None
        try:
            result = re.search(key,aline)
        except sre_compile.error as v:
            if key not in failed_keys:
                print('caught exception when attempting to find key = "{}" in line = "{}"'.format(key, aline))
                failed_keys += [ key ]
        if not result:
            return False
        if len(aline)<=result.start()+len(key)-1:
            return True
        nextchar = aline[result.start()+len(key)-1]
    else:
        index = aline.find(key)
        if index < 0:
            return False
        if len(aline)<=index+len(key):
            return False
        nextchar = aline[index+len(key)]

    if isMacroMatch.regexpstring.match(nextchar):
        #print "WARNING: skipping macro substitution to avoid possible mis-substitution"
        #print "WARNING:     key is \"%s\", delimiter is \"%s\", line is \"%s\""%(key,nextchar,re.sub("\n","",aline))
        return False
    return True
isMacroMatch.regexpstring = re.compile('[a-zA-Z0-9]')


def cleanBonus(bonus):
    if bonus:
        bonus = [ b for b in bonus if b.count("{")==b.count("}")]
    return bonus


def replaceMacros(dct, aline, verbose):
    # very expensive search? ;)
    # has become even more expensive thanks to macros with arguments!

    for k,v in ((k, dct[k]) for k in sorted(dct, key=len, reverse=True)):
        if isMacroMatch(aline,k):
            if not hasattr(v,"process_line"):
                aline = re.sub(k,v,aline)
            else:
                aline = v.process_line(aline)
            if verbose:
                if not replaceMacros.VeryVerbose: print("REPLACED MACRO {} with {}".format(k,v))
                else: print("REPLACED MACRO {} with {}: {}".format(k,v,aline))

    return aline
replaceMacros.VeryVerbose = False


style_files_so_far = ['sidecap.sty','lipsum.sty','authblk.sty','subfigure.sty','lineno.sty','tabulary.sty'] # List of packages to skip

class ProcessData():
        def __init__(self):
                self.left=0
                self.right=0
                self.holder = ''
                self.inputting = False
                self.figuring = False
                self.midfigure = False
                self.midcaption = False
                self.thecaption = ''
                self.thefigures = []
                self.sub_ref = []
                self.afig = ''
                self.tabling = False
                self.tab_tex = ''
                self.tab_begin = ''
    
############ Method for processing a latex file #################
def processFile(f=None,n=0,output=None,verbose=False,aux=False,noaux=False,rootPath='',macro_dict={},bonus=[],graphicsPath='',hundred=2,tab_n=1,tables=False,inc_cap=False,has_atlph=None,started_doc=False,refs=[],landscape=False, labels=None, data=ProcessData()):
    global chapters, chapter_no
    if verbose: print('Processing input file {} from number {:d} with verbose on and graphicsPath set to {}'.format(f,n,graphicsPath))
    attempts = 0
    fp = f
    global style_files_so_far
    global double
    while not os.access(fp,os.R_OK) and attempts<5:
        fp = fp[:fp.rfind('/',0,fp.rfind('/')-1)]+fp[fp.rfind('/'):]
        attempts += 1
        if attempts>0 and verbose:
            print('Attempted to patch tex file location to {}'.format(fp))
    if os.access(fp,os.R_OK): f=fp
    # Second attempt
    attempts=0
    fp = f.strip().replace(' ','')
    while not os.access(fp,os.R_OK) and attempts<5:
        fp = fp[f.find('/')+1:]
        attempts += 1
        if attempts>0 and verbose:
            print('Attempted to patch tex file location to {}'.format(fp))
    if os.access(fp,os.R_OK): f=fp

    if None==f or ''==f:
        if verbose: print('Attempted to process an empty file')
        return n,tab_n,aux
    if not os.access(f,os.R_OK):
        print('Could not locate input file {}'.format(f))
        return n,tab_n,aux
    if None==output:
        print('Please give me an output directory!')
        return n,tab_n,aux
    input = open(f,'r')

    # Only add to the existing macros
    macro_dict_new,bonus_new = extractMacros(input.read(), verbose)
    for a in macro_dict_new: 
        if not a in macro_dict:
            macro_dict[a]=macro_dict_new[a]
    if len(bonus_new)>0: bonus += bonus_new
    bonus = cleanBonus(bonus)

    # Check if we can update nested macros (macros which use macros)
    for key,value in macro_dict.items():
        if hasattr(value,"replace_sub_macros"):
            value.replace_sub_macros(macro_dict,verbose)

    # rewind after getting macros
    input.seek(0)

    # For block comments
    commented = False

    for theline in input.readlines():
        # Take care of comments in the latex
        aline = theline

        if '%' in aline:
            # Comments, but not escaped % marks!
            aline = aline.replace('\\%','QQQQQQ').split('%')[0].replace('QQQQQQ','\\%')+' '

        # Block comments
        if "\\begin{comment}" in aline: commented = True
        if "\\end{comment}" in aline: commented = False
        if commented == True: continue

        # Special case come upon in the Higgs paper; this was just used to make big ()
        aline = aline.replace('\\Bgl(','\\Big(')

        # Replace all macros
        aline = replaceMacros(macro_dict, aline, verbose)
        if double:
            if verbose: print('Second round of macro replacement')
            aline = replaceMacros(macro_dict, aline, verbose)

        if '%%FIGUREBREAK' in aline and data.figuring:
            if verbose: print('Force ending figure {}'.format(n))
            if not writeFiles( data.thecaption , data.thefigures , output , n , verbose , aux , hundred , data.sub_ref, labels=labels) and verbose:
                print('Had trouble writing out {} {} to {} for figure {}'.format(data.thecaption,data.thefigures,output,n))
            data.figuring = False
            data.midfigure = False
            data.midcaption = False
            n = n + 1
 
            if verbose: print('Forced to find a beginning of a figure!')
            if '\\end{' in aline:
                print('Found a line with both begin and end in it.  Your latex is too dense for me, sorry.  Giving up on this figure.')
                continue
            data.figuring = True
            data.thecaption = ''
            data.thefigures = []
            data.sub_ref = []
            continue
        if '\\usepackage' in aline and 'atlasphysics' in aline: 
            has_atlph = ''
            if '[' in aline: has_atlph=aline.split('[')[1].split(']')[0]

        # Swap out citations here!
        if '\\cite' in aline:
            while '\\cite' in aline:
                ref_string = ''
                ref_count = False
                full_ref_string = '\\cite'
                for c in aline.split('\\cite')[1]:
                    full_ref_string += c
                    if '{'==c:
                        ref_count=True
                        continue
                    if ref_count and '}'==c: break
                    if ref_count: ref_string+=c
                swap_string = '['
                for r,aref in enumerate(ref_string.split(',')):
                    if aref not in refs: refs += [ aref ]
                    if r!=0: swap_string += ','
                    swap_string += str(refs.index(aref)+1)
                swap_string += ']'
                aline = aline.replace(full_ref_string,swap_string)

        # Deal with chapter numbers and incrementing
        if '\\chapter' in aline and not '\\chapter*' in aline and chapters:
            ch_number = int(chapter_no.replace('_','').replace('ch',''))+1
            if ch_number>1: print('Beginning new chapter after {:d} figures and {:d} tables in chapter {}'.format(n-1,tab_n-1,ch_number-1))
            if verbose: print('Found chapter command; incrementing chapter')
            # Format: ch#_
            chapter_no = 'ch{02d}_'.format(ch_number)
            if verbose: print('Chapter lead now: {}'.format(chapter_no))
            # Reset numbering for new chapter
            n = 1
            tab_n = 1

        if data.inputting:
            # Middle of reading in the name of the next tex file
            if verbose: print('Continuing to process an input command!')
            if not '}' in aline:
                if verbose: print('Still processing an input line that wraps...')
                data.holder = data.holder + aline.strip()
                continue
            data.holder = data.holder+aline.split('}')[0]
            if not '.tex' in data.holder: # Case that they didn't put a .tex in
                data.holder = data.holder + '.tex'
            if not data.holder.startswith('/'): # Treat relative paths
                data.holder = f[:f.rfind('/')+1]
            (n,tab_n,aux) = processFile(data.holder,n,output,verbose,aux,noaux,rootPath,macro_dict,bonus,graphicsPath,hundred,tab_n,tables,inc_cap,has_atlph,started_doc,refs,landscape, labels=labels)
            data.inputting = False
        elif data.figuring:  # ZLM add subref counting
            # Middle of a figure
            if verbose: print('Continuing to process a figure')
            # Case: mid-caption
            if data.midcaption:
                if verbose: print('Still inside a caption {}'.format(data.thecaption))
                data.left = data.left + aline.count('{')
                data.right = data.right + aline.count('}')
                if data.left>data.right: # Still going!
                    data.thecaption += aline.strip()+' '
                    if verbose: print('Continuing to work on caption {}'.format(data.thecaption))
                    continue
                elif data.left==data.right: # Nicely formated, thanks!
                    if verbose: 
                        print('Found nice formatting. Cleaning line: {} to add to caption {}'.format(aline,data.thecaption))
                    data.thecaption += aline.strip()[:aline.strip().rfind('}')]
                    data.midcaption = False
                    if verbose:
                        print('Done working on caption {} from line {}'.format(data.thecaption,aline))
                else:
                    # Now we're done before the end of the line...
                    data.thecaption += aline[:capline.rfind('}')]
                    if verbose: 
                        print('Will try to trim caption {} by at least {:d} cases of \}'.format(data.thecaption,data.right-data.left))
                    data.left = data.thecaption.count('{')
                    data.right = data.thecaption.count('}')
                    while data.left<data.right:
                        data.thecaption = capline[:capline.rfind('}')]
                        data.right = data.thecaption.count('}')
                    data.midcaption = False
                    if verbose: print('Done with caption {}'.format(data.thecaption))
                    # Done finding that... bit of a trick, huh?
            # Case: mid-figure input
            elif data.midfigure:
                if verbose: print('Still working on a figure input {}'.format(data.afig))
                if not '}' in aline:
                    data.afig += aline.strip()
                    continue
                data.afig += aline.split('}')[0]
                thefig = findFigure( data.afig.replace('{','').replace('}','').strip() , rootPath , graphicsPath , None , verbose )
                data.thefigures += [thefig]
                data.midfigure = False
            # Case: unhandled mbox input
            if '\\mbox' in aline and ('.eps' in aline or '.pdf' in aline or '.png' in aline or '.jpg' in aline):
                if not 'includegraphics' in aline:
                    print('WARNING: Please use \\includegrapics for including graphics.  \\mbox not yet supported.  Will skip a figure.')
                else:
                    aline = aline.split('\\mbox{')[1]
                    br_diff = aline.count('}') - aline.count('{')
                    for a in range(br_diff) : aline = aline[:aline.rfind('}')]
            # Case: Figure input
            if '\\includegraphics' in aline:
                if verbose: print('Found a figure for inputting!')
                if len(aline.split('\\includegraphics'))>2:
                    # Ensure that figures begin and end on the same line if we are doing multi-figures per line
                    if aline.count('{') != aline.count('}'):
                        print('More than one include graphics command on one line, while not terminated on the same line, is not yet supported')
                    else:
                        # Get the tokens and determine which are valid
                        tokens = aline.split(r'\\includegraphics')
                        validTokens = []
                        for aToken in tokens:
                            if aToken.count('{') == 1 and aToken.count('}') == 1:
                                if ".eps" in aToken or ".pdf" in aToken or ".png" in aToken or ".jpg" in aToken:
                                    validTokens.append(aToken)

                        if verbose: print('Found a figure with {:d} parts for inputting'.format(len(validTokens)))

                        # Now retrieve the file names
                        fileNames = []
                        for aToken in validTokens:
                            subTokens = re.sub('{','QQQQQ',re.sub('}','QQQQQ',aToken)).split('QQQQQ')
                            for aSubToken in subTokens:
                                # Find the first one that contains an expected extension
                                if ".eps" in aSubToken or ".pdf" in aSubToken or ".png" in aSubToken or ".jpg" in aSubToken:
                                    fileNames.append(aSubToken)
                        
                        # Now store the figures
                        for aFileName in fileNames:
                            data.thefigures += [findFigure(aFileName.strip(),rootPath,graphicsPath,None,verbose)]

                else:
                    partline = aline.split('\\includegraphics')[1]
                    pageN = None
                    if partline.strip().startswith('['):
                        # Handle sub-figure from pdf document
                        if 'page=' in partline.split(']')[0]:
                            pageN = partline.split('page=')[1].split(']')[0].split(',')[0]
                        partline=partline[ partline.find(']')+1 : ] .strip()
                    partline = partline[ partline.find('{')+1 : ].strip()
                    if '}' in partline: # End figure on the same line, thanks
                        thefig = findFigure( partline[:partline.find('}')].replace('{','').replace('}','').strip() , rootPath , graphicsPath , pageN , verbose )
                        data.thefigures += [thefig]
                    else: # Multi-line read in...
                        data.afig = partline
                        data.midfigure = True
                        continue
            # Case: eps figure input
            if '\\epsfig' in aline:
                if verbose: print('Found a figure for inputting!')
                if 'file=' in aline:
                    thefig = findFigure( aline.split('file=')[1].split(',')[0].split('}')[0] , rootPath , graphicsPath , verbose )
                    data.thefigures += [thefig]
                elif 'figure=' in aline:
                    thefig = findFigure( aline.split('figure=')[1].split(',')[0].split('}')[0] , rootPath , graphicsPath , verbose )
                    data.thefigures += [thefig]
                # Those are the only two examples I've seen so far...
            if '\\input{' in aline and '.tex' in aline:
                print('WARNING: Spotted \\input command inside a figure section.  Not yet supported.')
            # Case: starting the caption
            # @TODO: make this a bit more clever - should recognize certain allowed characters after caption or icaption and not start on a command like '\captionblahblahblah'
            if '\\label' in aline:
                lab_text = ''
                started_sub_ref = False
                for c in aline.split('\\label')[1]:
                    if '{'==c:
                        started_sub_ref = True
                        continue
                    elif '}'==c:
                        break
                    elif started_sub_ref:
                        lab_text+=c
                data.sub_ref += [ lab_text.strip() ]
            if '\\caption' in aline or '\\icaption' in aline:
                if verbose: print('Beginning caption for figure {:d}'.format(n))
                if '\\icaption' in aline: capline = aline.split('\\icaption')[1]
                elif '\\caption' in aline: capline = aline.split('\\caption')[1]
                if verbose: print('Examining caption {}'.format(capline))
                # Special case: try to remove 'label' if it comes after the closing } and is all a single line
                labpos = capline.find('\\label')
                if labpos>0 and capline[:labpos].count('{')==capline[:labpos].count('}'): # had a label
                    capline = capline[:labpos]
                # Special case: try to handle 'short' captions
                if capline.find('[')>=0 and capline.find(']')>=0 and capline.find('{')>capline.find('['):
                    capline = capline[capline.find(']')+1:]
                # Special case: ignore an empty caption{} command which may be that of a sub-figure
                if capline.rstrip().replace(' ','') == '{}':
                    if verbose: print('Ignoning empty caption assuming it is from a sub-figure')
                    continue
                if verbose: print('Found to be non-empty: {}'.format(capline))
                data.left = capline.count('{')
                data.right = capline.count('}')
                # Two cases: entire caption on one line, or caption broken into several lines...
                #   Second part handles the case of a caption command followed by the caption on the next line
                if data.left>data.right or len(capline.strip())<1:
                    data.midcaption=True
                    data.thecaption = capline[1:].strip()+' '
                    if verbose: 
                        print('Moving on with caption: {}'.format(data.thecaption))
                        print('With l,r {} {} From {}'.format(data.left,data.right,capline))
                        print('Beginning from {}'.format(aline))
                    continue
                elif data.left==data.right: # Nicely formatted, thank you
                    data.thecaption = capline[1:capline.rfind('}')]
                else:
                    data.thecaption = capline[1:capline.rfind('}')]
                    data.left = data.thecaption.count('{')
                    data.right = data.thecaption.count('}')
                    while data.left<data.right:
                        data.thecaption = capline[:capline.rfind('}')]
                        data.right = data.thecaption.count('}')
                    # Done finding that... bit of a trick, huh?
            # Case: ending the figure
            if '\\end{' in aline and ('figure' in aline and not 'subfigure' in aline):
                if verbose: print('Ending figure {:d}'.format(n))
                if not writeFiles( data.thecaption , data.thefigures , output , n , verbose , aux , hundred , data.sub_ref, labels=labels ):
                    if verbose: print('Had trouble writing out {} {} to {} for figure {:d}'.format(data.thecaption,data.thefigures,output,n))
                data.figuring = False
                data.midfigure = False
                data.midcaption = False
                n = n + 1
            pass
        elif '\\input{' in aline:
            # Found a command to read in another tex file
            if verbose: print('Found an input command, will process the next file')
            if data.tabling: print('Found an include command inside of a table.  Probably will not succeed to parse this')
            if len(aline.split('\\input{'))>2:
                print('WARNING: Only one \input per line please! Only the first will be processed. {}'.format(aline.strip()))
            data.holder = aline.split('\\input{')[1].strip()
            if not '}' in data.holder:
                if verbose: print('Recognized input line that wraps...')
                data.inputting = True
                continue
            data.holder = data.holder.split('}')[0]
            if not '.tex' in data.holder: # Case that they didn't put a .tex in
                data.holder = data.holder + '.tex'
            if not data.holder.startswith('/'): # Treat relative paths
                data.holder = f[:f.rfind('/')+1] + data.holder
            #rootPath = rootPath+data.holder[:data.holder.rfind('/')+1]
            (n,tab_n,aux) = processFile(data.holder,n,output,verbose,aux,noaux,rootPath,macro_dict,bonus,graphicsPath,hundred,tab_n,tables,inc_cap,has_atlph,started_doc,refs,landscape, labels=labels)
        elif '\\input ' in aline:
            # Found a command to read in another tex file
            if verbose: print('Found an input command, will process the next file')
            if data.tabling: print('Found an include command inside of a table.  Probably will not succeed to parse this')
            if len(aline.split('\\input'))>2:
                print('WARNING: Only one \input per line please! Only the first will be processed. {}'.format(aline.strip()))
            data.holder = aline.split('\\input')[1].split()[0].strip() # Needs to be the one after the \input
            if not '.tex' in data.holder: # Case that they didn't put a .tex in
                data.holder = data.holder + '.tex'
            if not data.holder.startswith('/'): # Treat relative paths
                data.holder = f[:f.rfind('/')+1] + data.holder
            #rootPath = rootPath+data.holder[:data.holder.rfind('/')+1]
            (n,tab_n,aux) = processFile(data.holder,n,output,verbose,aux,noaux,rootPath,macro_dict,bonus,graphicsPath,hundred,tab_n,tables,inc_cap,has_atlph,started_doc,refs,landscape, labels=labels)
        elif '\\include{' in aline:
            # Found a command to read in another tex file
            if verbose: print('Found an include command, will process the next file')
            if data.tabling: print('Found an include command inside of a table.  Probably will not succeed to parse this')
            if len(aline.split('\\include{'))>2:
                print('WARNING: Only one \include per line please! Only the first will be processed. {}'.format(aline.strip()))
            data.holder = aline.split('\\include{')[1].strip()
            if not '}' in data.holder:
                if verbose: print('Recognized include line that wraps...')
                data.inputting = True
                continue
            data.holder = data.holder.split('}')[0]
            if not '.tex' in data.holder: # Case that they didn't put a .tex in
                data.holder = data.holder + '.tex'
            if not data.holder.startswith('/'): # Treat relative paths
                data.holder = f[:f.rfind('/')+1] + data.holder
            #print('Running with',data.holder,rootPath)
            (n,tab_n,aux) = processFile(data.holder,n,output,verbose,aux,noaux,rootPath,macro_dict,bonus,graphicsPath,hundred,tab_n,tables,inc_cap,has_atlph,started_doc,refs,landscape, labels=labels)
        elif '\\include*{' in aline:
            # Found a command to read in another tex file
            if verbose: print('Found an include* command, will process the next file')
            if data.tabling: print('Found an include command inside of a table.  Probably will not succeed to parse this')
            if len(aline.split('\\include*{'))>2:
                print('WARNING: Only one \include* per line please! Only the first will be processed. {}'.format(aline.strip()))
            data.holder = aline.split('\\include*{')[1].strip()
            if not '}' in data.holder:
                if verbose: print('Recognized include* line that wraps...')
                data.inputting = True
                continue
            data.holder = data.holder.split('}')[0]
            if not '.tex' in data.holder: # Case that they didn't put a .tex in
                data.holder = data.holder + '.tex'
            if not data.holder.startswith('/'): # Treat relative paths
                data.holder = f[:f.rfind('/')+1] + data.holder
            #print('Running with',data.holder,rootPath)
            (n,tab_n,aux) = processFile(data.holder,n,output,verbose,aux,noaux,rootPath,macro_dict,bonus,graphicsPath,hundred,tab_n,tables,inc_cap,has_atlph,started_doc,refs,landscape, labels=labels)
        elif '\\usepackage' in aline:
            if verbose: print('Attempting to find a usepackage locally')
            the_pkg = aline.split('{')[1].split('}')[0]+'.sty'
            if the_pkg.split('/')[-1] in style_files_so_far:
                if verbose: print('Skipping style file {}'.format(the_pkg))
                continue
            else:
                if verbose: print('Processing style file {} {}'.format(the_pkg,style_files_so_far))
            style_files_so_far += [the_pkg.split('/')[-1]]
            if not 'atlascontribute' in the_pkg and not 'atlascover' in the_pkg and not 'jheppub.sty' in the_pkg:
                if not the_pkg.startswith('/'):
                    the_pkg = f[:f.rfind('/')+1] + the_pkg
                (n,tab_n,aux) = processFile(the_pkg,n,output,verbose,aux,noaux,rootPath,macro_dict,bonus,graphicsPath,hundred,tab_n,tables,inc_cap,has_atlph,started_doc,refs,landscape, labels=labels)
        # Note: catch added to avoid treating SI mode as a figure begin...
        elif '\\begin{' in aline and ('figure' in aline and not 'subfigure' in aline) and not 'mode=figures' in aline and not 'table-figures-uncertainty' in aline:
            # Found a command to begin inputting some figures
            if not started_doc:
                print('Found a figure, but have not yet found a \\begin{document} command.  Skipping this one...')
                continue
            if verbose: print('Found a beginning of a figure!')
            if '\\end{' in aline:
                print('Found a line with both begin and end in it.  Your latex is too dense for me, sorry.  Giving up on this figure.')
                continue
            data.figuring = True
            data.thecaption = ''
            data.thefigures = []
            data.sub_ref = []
        # Look for common markers for auxilliary material
        elif ('\\part*' in aline or '\\section' in aline) and ('additional material' in aline.lower() or ('aux' in aline.lower() and 'mat' in aline.lower())):
            if not noaux and not aux:
                print('Done processing {:d} figures and {:d} tables in main text. Moving to auxiliary material.'.format(n-1,tab_n))
                aux = True
                n = 1
                tab_n = 1
        elif '\\documentclass{atlasnote}' in aline and not noaux:
            print('WARNING: Looks like you are processing a CONF note.  Disabling auxiliary plots.')
            noaux = True
        elif tables and not data.tabling and ('\\begin' in aline and ('tabular' in aline or 'table' in aline)) and not aline.strip().startswith('\\newcommand'):
            data.tabling = True
            if not 'table' in aline or 'longtable' in aline: data.tab_tex += aline
            if 'tabular' in aline: data.tab_begin = 'tabular'
            elif 'table' in aline: data.tab_begin = 'table'
        elif tables and data.tabling:
            # Note the special catch for notes...
            if '\\end' in aline and data.tab_begin in aline and not (data.tab_begin=='table' and ('tablenotes' in aline or 'threeparttable' in aline)):
                if not 'table' in aline or 'longtable' in aline: data.tab_tex += aline
                if ''==data.tab_tex.strip():
                    print('Appears to have been an empty table... Skipping, as this probably just means you had confusing tex')
                else:
                    make_table( data.tab_tex , output , tab_n , aux , hundred , inc_cap , has_atlph , landscape , bonus , verbose)
                    tab_n += 1
                data.tabling = False
                data.tab_begin = ''
                data.tab_tex = ''
            else:
                # A little ad hoc cleaning here...
                # This replacement breaks \si{\GeV} so let's replace that too
                bline = theline.replace(r'\si{\GeV}','GeV').replace(r'\si{\MeV}','MeV')
                bline = bline.replace(r'\GeV','GeV').replace(r'\MeV','MeV').replace(r'\Bgl(',r'\Big(')
                bline = replaceMacros(macro_dict, bline, verbose)
                if '%' in bline:
                    # Comments, but not escaped % marks!
                    bline = bline.replace('\\%','QQQQQQ').split('%')[0].replace('QQQQQQ','\\%')+' '
                # Swap out citations here!
                if '\\cite' in bline:
                    while '\\cite' in bline:
                        ref_string = ''
                        ref_count = False
                        full_ref_string = '\\cite'
                        for c in bline.split('\\cite')[1]:
                            full_ref_string += c
                            if '{'==c:
                                ref_count=True
                                continue
                            if ref_count and '}'==c: break
                            if ref_count: ref_string+=c
                        swap_string = '['
                        for r,aref in enumerate(ref_string.split(',')):
                            if aref not in refs: refs += [ aref ]
                            if r!=0: swap_string += ','
                            swap_string += str(refs.index(aref)+1)
                        swap_string += ']'
                        bline = bline.replace(full_ref_string,swap_string)
                data.tab_tex += bline # Here so that we can skip it under the right circumstances
        elif '\\begin' in aline and 'document' in aline:
            started_doc = True
        elif '\\begin' in aline and 'landscape' in aline:
            landscape = True
        elif '\\end' in aline and 'landscape' in aline:
            landscape = False
        elif '\\endinput' in aline:
            return n,tab_n,aux
        if '\\graphicspath' in aline: # and graphicsPath is '':
            graphicsPath = aline.split('\\graphicspath')[1]
            multiPath = (graphicsPath.count('{')!=graphicsPath.count('}') or graphicsPath.count('{')>1)
            if verbose and multiPath:
                print('Warning: will not handle {} in graphics paths very carefully...')
            if multiPath:
                if verbose:
                    print('graphicsPath on input: {}'.format(graphicsPath))
                    # graphicsPath = graphicsPath.strip().replace('{{','').replace('}}','').replace('}{',' ')
                graphicsPath = re.sub('\{\s*\{|\}\s*\{|\}\s*\}',' ',graphicsPath)
                if verbose:
                    print('Warning: using experimental multiPath mode')
                    print('setting graphicsPath to {} as defined in file {}'.format(graphicsPath, f))
            else:
                graphicsPath = graphicsPath.strip().replace('{','').replace('}','')
                print('setting graphicsPath to {} as defined in file {}'.format(graphicsPath, f))

    # Done processing all the lines in the file!
    return n,tab_n,aux

the_cwd = os.getcwd()
if '/' in inputFile:
    os.chdir(inputFile[:inputFile.rfind('/')])
    inputFile = inputFile.split('/')[-1]
    outputFile = the_cwd+'/'+outputFile

labels = parse_labels(guess_aux_filename(inputFile), verbose)

(total,tab_total,aux) = processFile(inputFile,number,outputFile,verbose,aux,noaux,inputFile[:inputFile.rfind('/')+1],{},[],'',hundred,1,tables,inc_cap,None,False,[],False,labels=labels)
if total<0:
    print('Ending on an error.')
elif aux:
    print('All done processing {:d} figures and {:d} tables in auxiliary material'.format(total-1,tab_total-1))
elif not chapters:
    print('All done processing {:d} figures and {:d} tables in the main text'.format(total-1,tab_total-1))

if len(failed_keys) > 0:
    print('note that the following {:d} macros could not be properly resolved:'.format(len(failed_keys)))
    for k in failed_keys: print('\t {}'.format(k))

os.chdir(the_cwd)
